What is it?
===========

LFS Certificates is pluggable application for use with LFS (Lighting Fast Shop) that makes it possible to create product certificates


Basic usage
===========

* add certificates to INSTALLED_APPS in your settings.py
* add certificates urls to your site urls.py like:

  from certificates.views import CertificatesSite
  (r'^certificates/', include(CertificatesSite.urls)),

  
