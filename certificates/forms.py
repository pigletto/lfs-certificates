# -*- coding: utf-8 -*-
from certificates.models import LFSCertificate
from django.forms.models import ModelForm
from django.forms.widgets import HiddenInput
from lfs.core.widgets.image import LFSImageInput


class CertificateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CertificateForm, self).__init__(*args, **kwargs)
        self.fields["image"].widget = LFSImageInput()
        if not (self.instance and self.instance.pk):
            del(self.fields['position'])

    class Meta:
        model = LFSCertificate
        exclude = ('products', )
