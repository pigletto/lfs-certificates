# -*- coding: utf-8 -*-
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings
from lfs.catalog.models import Product

from lfs.catalog.settings import THUMBNAIL_SIZES
from lfs.core.fields.thumbs import ImageWithThumbsField

# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.db import models
from management_items.models import BaseManagementItem


class LFSCertificateManager(models.Manager):

    def active(self):
        return self.filter(is_active=True)


class LFSCertificate(BaseManagementItem):
    view_uid = 'certificates'
    title = models.CharField(_(u"Title"), null=False, blank=True, max_length=100, default='')
    slug = models.SlugField(_(u"Slug"), max_length=100)
    is_active = models.BooleanField(_('Active'), null=False, default=True)
    image = ImageWithThumbsField(_(u"Image"), upload_to="certificates", blank=True, null=True, sizes=THUMBNAIL_SIZES)
    body = models.TextField(_(u"Description"), blank=True, null=False, default='')
    position = models.PositiveIntegerField(_(u"Position"), default=99999)
    products = models.ManyToManyField(Product, verbose_name=_('Products'), related_name='certificates')

    objects = LFSCertificateManager()

    class Meta:
        ordering = ("position", )

    def __unicode__(self):
        return self.title


if 'south' in settings.INSTALLED_APPS:
    # south rules
    rules = [
      (
        (ImageWithThumbsField,),
        [],
        {
            "sizes": ["sizes", {"default": None}]
        },
      )
    ]
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules(rules, ["^lfs\.core\.fields\.thumbs"])
