# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting field 'LFSCertificate.text'
        db.delete_column('certificates_lfscertificate', 'text')

        # Adding field 'LFSCertificate.slug'
        db.add_column('certificates_lfscertificate', 'slug', self.gf('django.db.models.fields.SlugField')(default='blah', max_length=100, db_index=True), keep_default=False)

        # Adding field 'LFSCertificate.body'
        db.add_column('certificates_lfscertificate', 'body', self.gf('django.db.models.fields.TextField')(default='', blank=True), keep_default=False)


    def backwards(self, orm):
        
        # Adding field 'LFSCertificate.text'
        db.add_column('certificates_lfscertificate', 'text', self.gf('django.db.models.fields.TextField')(default='', blank=True), keep_default=False)

        # Deleting field 'LFSCertificate.slug'
        db.delete_column('certificates_lfscertificate', 'slug')

        # Deleting field 'LFSCertificate.body'
        db.delete_column('certificates_lfscertificate', 'body')


    models = {
        'certificates.lfscertificate': {
            'Meta': {'ordering': "('position',)", 'object_name': 'LFSCertificate'},
            'body': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'blank': 'True', 'max_length': '100', 'null': 'True', 'sizes': '((60, 60), (100, 100), (200, 200), (300, 300), (400, 400))'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '999'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '100', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['certificates']
