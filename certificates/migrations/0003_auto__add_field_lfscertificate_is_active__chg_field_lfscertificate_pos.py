# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'LFSCertificate.is_active'
        db.add_column('certificates_lfscertificate', 'is_active', self.gf('django.db.models.fields.BooleanField')(default=True), keep_default=False)

        # Changing field 'LFSCertificate.position'
        db.alter_column('certificates_lfscertificate', 'position', self.gf('django.db.models.fields.PositiveIntegerField')())


    def backwards(self, orm):
        
        # Deleting field 'LFSCertificate.is_active'
        db.delete_column('certificates_lfscertificate', 'is_active')

        # Changing field 'LFSCertificate.position'
        db.alter_column('certificates_lfscertificate', 'position', self.gf('django.db.models.fields.PositiveSmallIntegerField')())


    models = {
        'certificates.lfscertificate': {
            'Meta': {'ordering': "('position',)", 'object_name': 'LFSCertificate'},
            'body': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'blank': 'True', 'max_length': '100', 'null': 'True', 'sizes': '((60, 60), (100, 100), (200, 200), (300, 300), (400, 400))'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '99999'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '100', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['certificates']
