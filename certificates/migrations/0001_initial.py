# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'LFSCertificate'
        db.create_table('certificates_lfscertificate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True)),
            ('image', self.gf('lfs.core.fields.thumbs.ImageWithThumbsField')(blank=True, max_length=100, null=True, sizes=((60, 60), (100, 100), (200, 200), (300, 300), (400, 400)))),
            ('text', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=999)),
        ))
        db.send_create_signal('certificates', ['LFSCertificate'])


    def backwards(self, orm):
        
        # Deleting model 'LFSCertificate'
        db.delete_table('certificates_lfscertificate')


    models = {
        'certificates.lfscertificate': {
            'Meta': {'ordering': "('position',)", 'object_name': 'LFSCertificate'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'blank': 'True', 'max_length': '100', 'null': 'True', 'sizes': '((60, 60), (100, 100), (200, 200), (300, 300), (400, 400))'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '999'}),
            'text': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['certificates']
