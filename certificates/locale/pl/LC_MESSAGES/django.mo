��          �      |      �     �     �                    +     G     e     �     �     �     �     �  .   �     �  G        L     U     ^     o     t    z          �     �     �  
   �     �     �     �  !     /   3     c     o     �  ,   �     �  r   �     +     3     <     N     R     	                                                                 
                           Active Add Certificate Add certificate Cancel Certificate Certificate has been added. Certificate has been deleted. Certificate has been saved. Certificate set Certificate unset Certificates Delete Certificate Description Do you really want to delete this certificate? Image No certificates defined yet. Add some: <a href="%(certs_url)s">here</a> Position Products Save certificate Slug Title Project-Id-Version: Savonnoir
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-02-12 22:59+0100
PO-Revision-Date: 2012-02-12 23:00+0100
Last-Translator: Maciej Wiśniowski <pigletto@gmail.com>
Language-Team: natcam.pl <hello@natcam.pl>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
X-Poedit-Language: Polish
X-Poedit-Country: POLAND
X-Poedit-SourceCharset: utf-8
 Aktywny Dodaj certyfikat Dodaj certyfikat Anuluj Certyfikat Certyfikat został dodany. Certyfikat został usunięty. Certyfikat został zapisany. Przypisano certyfikat do produktu Certyfikat nie jest już przypisany do produktu Certyfikaty Usuń certyfikat Opis Czy na pewno chcesz usunąć ten certyfikat? Obraz Nie zdefiniowano jeszcze żadnych certyfikatów. Kliknij <a href="%(certs_url)s">tutaj</a> aby dodać certyfikaty. Pozycja Produkty Zapisz certyfikat URL Tytuł 