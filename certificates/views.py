# -*- coding: utf-8 -*-
# django imports
from django.utils import simplejson
from django.utils.translation import ugettext as _
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from lfs.caching import update_product_cache

from lfs.catalog.models import Product

from management_items.site import LFSBaseManagementView

from .models import LFSCertificate
from .forms import CertificateForm

class LFSCertificateView(LFSBaseManagementView):
    view_uid = 'certificates'
    template_update_item = "certificates/manage/update_item.html"
    template_add_item = "certificates/manage/add_item.html"

    # CLASS getters
    def get_model_cls(self):
        return LFSCertificate

    def get_add_form_cls(self):
        """ Return form used by add view """
        return CertificateForm
    # /CLASS getters

    # MESSAGES
    def get_item_updated_message(self, item):
        return _(u"Certificate has been saved.")

    def get_item_added_message(self, item):
        return _(u"Certificate has been added.")

    def get_item_deleted_message(self, item):
        return _(u"Certificate has been deleted.")
    # /MESSAGES

    def set_cert_4_product(self, request, product_id, certificate_id):
        """ Set/unset certificate for specific product
        """
        ret = {'message': 'Invalid request'}
        if request.POST:
            cert = get_object_or_404(self.get_model_cls(), pk=certificate_id)
            product = get_object_or_404(Product, pk=product_id)

            checked = bool(int(request.POST.get('checked', 0)))

            if not checked and cert in product.certificates.filter(is_active=True):
                product.certificates.remove(cert)
                ret = {'message': _('Certificate unset')}
            elif checked:
                product.certificates.add(cert)
                ret = {'message': _('Certificate set')}
            update_product_cache(product)
        return HttpResponse(simplejson.dumps(ret))

    def view_item(self, request, id):
        cert = get_object_or_404(self.get_model_cls(), pk=id, is_active=True)
        return render(request, 'certificates/certificate.html', {'certificate': cert})

    def get_urls(self):
        from django.conf.urls.defaults import patterns, url
        urlpatterns = super(LFSCertificateView, self).get_urls()
        wrap = self.get_wrap()
        urlpatterns += patterns('',
            url(r'^lfs_certificates_4_product/(?P<product_id>\d+)/(?P<certificate_id>\d+)/$',
                wrap(self.set_cert_4_product), name="lfs_certificates_4_product"),
        )
        return urlpatterns

CertificatesSite = LFSCertificateView()
