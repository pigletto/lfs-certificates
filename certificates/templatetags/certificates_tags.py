# -*- coding: utf-8 -*-

# django imports
from django.template import Library

from django.utils.safestring import mark_safe

from certificates.views import CertificatesSite

register = Library()


@register.inclusion_tag('certificates/manage/manage_product_tab.html', takes_context=True)
def certificates_management(context, product):
    all_certs = CertificatesSite.get_model_cls().objects.filter(is_active=True)
    assigned = product.certificates.values_list('pk', flat=True)
    return {'certificates': all_certs, 'assigned': assigned, 'product': product}


@register.inclusion_tag('certificates/render_certificates.html', takes_context=True)
def render_certificates(context, product):
    certificates = product.certificates.active()
    return {'certificates': certificates, 'product': product}